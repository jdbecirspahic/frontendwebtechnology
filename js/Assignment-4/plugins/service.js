const util = require('util');
const fs = require('fs');
const readFile = util.promisify(fs.readFile);
const fp = require('fastify-plugin');

async function readEnzymeData(){
    let data = await readFile('../../data/enzyme-sites.txt', 'utf8');
    data = data.split("\n");
    return parseEnzymeData(data);
}

function parseEnzymeData(data){
    let enzymeMap = new Map();
    for (let rowIndex = 1; rowIndex < data.length; rowIndex++){
        let row = data[rowIndex];
        let regex = new RegExp("(\\w+)\\s(\\([A-Za-z0-9\.]+\\))?\\s+([ACGTNRYMKSWBDHV\^]+)")
        let match = row.match(regex);
        if (!enzymeMap.has(`${match[1]}`)){
            if (`${match[2]}` !== "undefined"){
                enzymeMap.set(`${match[1]}`, {"name": `${match[1]}`, "synonym": `${match[2].substring(1, match[2].length-1)}`,
                    "sites": [`${match[3]}`]});
            }else {
                enzymeMap.set(`${match[1]}`, {"name":`${match[1]}`, "synonym": "", "sites": [`${match[3]}`]})
            }

        }else {
            let previousSites = enzymeMap.get(`${match[1]}`).sites;
            previousSites.push(`${match[3]}`)

            enzymeMap.set(`${match[1]}`,
                {"name": `${match[1]}`,
                "synonym": enzymeMap.get(`${match[1]}`).synonym,
                "sites": previousSites})
        }
    }
    return enzymeMap;
}

getOligoCutSites = async function (oligo, enzymeMap) {
    let enzymes = await enzymeMap;
    let cuttingEnzymes = [];
    for (let enzyme of enzymes.values()) {
        let locations = [];
        for (let site of enzyme.sites){
            let regexSite = site.toUpperCase()
                .replace("^", "")
                .replaceAll("B", "[CGT]")
                .replaceAll("D", "[AGT]")
                .replaceAll("H", "[ACT]")
                .replaceAll("K", "[GT]")
                .replaceAll("M", "[AC]")
                .replaceAll("N", "[ACGT]")
                .replaceAll("R", "[AG]")
                .replaceAll("S", "[CG]")
                .replaceAll("V", "[ACG]")
                .replaceAll("W", "[AT]")
                .replaceAll("Y", "[CT]")

            let pattern = new RegExp(regexSite, 'g');
            let matchArray = oligo.matchAll(pattern);

            for (let match of matchArray){
                locations.push(match.index);
            }
        }
        cuttingEnzymes.push({
            enzyme: enzyme.name,
            site: enzyme.sites,
            locations: locations
        })
    }
    return cuttingEnzymes;
}

// getOligoCutSites("ATGACTGTGCAGTAGTACGTGTACGCGACGTGCAATCGATCGATCG", readEnzymeData()).then(console.log);

module.exports = fp(async function (fastify, opts){
    fastify.decorate("readEnzymeData", readEnzymeData);
})