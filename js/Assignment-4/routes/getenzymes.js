

async function routes(fastify, opts){
    let enzymeMap = await fastify.readEnzymeData();

    fastify.get('/api/v3/enzymes/:enzyme', async (request, reply) =>{
        reply.send(
            enzymeMap.get(request.params.enzyme)
        );
    });

    fastify.get('/client', async (request, reply) => {
        return reply.sendFile('Assigment-4.html');
    });

    fastify.post('/api/v3/getcuttingsites', async (request, reply) => {
        let oligo = JSON.parse(request.body).oligo;
        if (oligo.length > 50 || oligo.length < 1){
            reply.code(400);
            reply.send("Please submit a sequence that is between 1 and 50 nucleotides long.");
        }
        reply.code(200)
        reply.send(
            await getOligoCutSites(oligo, enzymeMap)
        )
    });
}

module.exports = routes;