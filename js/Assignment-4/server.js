// IMPORTS
const path = require("path");
const AutoLoad = require('fastify-autoload')
const fastify = require('fastify')({
    logger: true
});
// ROUTES
fastify.register(require('./routes/getenzymes'), {prefix: "/api/v3/enzyme"});
// Place here your custom code!
fastify.register(require('fastify-static'), {
    root: path.join(__dirname, 'Assignment-4.html'),
    prefix: '/public/',
    decorateReply: true
})

fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'plugins'),
    options: Object.assign({}, opts)
})

// LISTENER
const start = async () => {
    await fastify.listen(8000, function (err, address) {
        if (err) {
            fastify.log.error(err)
            process.exit(1)
        }
        fastify.log.info(`server listening on ${address}`)
    })
}
start();
