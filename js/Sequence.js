Sequence = {
    // Starting the sequence with a start codon.
    sequence : "ATG",
    // char occurrences will hold all unique characters in the sequence and count their occurrence
    charOccurrences : {},

    // Creates random nucleotide sequence
    createSequence : function (length){
        const nucleotides = ["A", "T", "G", "C"];
        const stopCodons = ["TAA", "TAG", "TGA"];

        const sequenceLength = this.checkLength(length)
        // making random sequence. i < length -6 to account for the addition of start and stop codon
        for (let i = 0; i < sequenceLength - 6; i++){
            this.sequence += nucleotides[Math.floor(Math.random() * nucleotides.length)];
        }

        // adding a stop codon to the sequence
        this.sequence += stopCodons[Math.floor(Math.random() * stopCodons.length)];
    },

    // returns sequence with line lengths of 70 characters. Used for setting the innerHTML for an element.
    getFormattedSequence: function (){
        let formattedSequence = "<p style='text-align: justify; text-justify: inter-character'>";
        for (let i = 0; i < this.sequence.length; i++){
            if (i % 70 === 0 && i !== 0){
                formattedSequence += "<br>";
            }
            formattedSequence += this.sequence[i];
        }
        formattedSequence += "</p>"
        return formattedSequence;
    },

    // Checks whether the given length is divisible by 3 and corrects it if its not.
    checkLength : function (length){
        if (length % 3 === 0){
            return length;
        }
        if (length % 3 === 1){
            return length-1;
        }
        if (length % 3 === 2){
            return length-2;
        }
    },

    getGc : function (){
        let gcCount = 0;
        // for every nucleotide check whether it's a G or C
        for (let i = 0; i < this.sequence.length; i++){
            if (this.sequence[i] === "G" || this.sequence[i] === "C"){
                gcCount ++;
            }
        }
        return (gcCount/this.sequence.length) * 100;
    },

    // returns the length of the sequence if it was translated to an amino acid sequence
    getProteinLength : function (){
        return this.sequence.length/3;
    },

    // iterates through the sequence and adds each unique character to a set. If character is already in the set,
    // increases count by 1.
    countCharOccurrences : function (){
        for (let i = 0; i < this.sequence.length; i++){
            // if character is not in sequenceChars, add it and set its value to 0
            if (!Object.keys(this.charOccurrences).includes(this.sequence[i])){
                this.charOccurrences[this.sequence[i]] = 1;
            }
            else{
                this.charOccurrences[this.sequence[i]] ++;
            }
        }
    },

    getOccurrence : function (nucleotide){
        return this.charOccurrences[nucleotide];
    },

    getOccurrencePercentage : function (nucleotide){
        return this.charOccurrences[nucleotide]/this.sequence.length * 100;
    },

    // returns statistics in the form of a string that can be used as HTML code
    getFormattedStatistics : function (){
        let statistics = "<p>Sequence Length: " + this.sequence.length + " </p>" +
            "<p>GC Percentage: " + this.getGc() + "%" + "</p>" +
            "<p>Protein Length: " + this.getProteinLength() + " Amino Acids" + "</p>";
        // Iterate over keys, for every key get its absolute and percentage occurrence
        const keys = Object.keys(this.charOccurrences)
        keys.forEach((char) => {
            statistics += "<p>"+char+" occurrence: " + this.getOccurrence(char) +
                " / " + this.getOccurrencePercentage(char) + "%" + "</p>";
        })
        return statistics;
    },

    // for each cutting site of the enzyme, gets all the positions and returns an HTML <p> with text containing
    // information about the the enzyme
    getCutSites : function (enzymeSites, enzymeName){
        let subParagraph = "<p>Enzyme name: " + enzymeName + "<br> Cutting sites: " + enzymeSites + "<br>";
        let numberOfMatches = 0;
        let positionArray = [];
        for(let i = 0; i < enzymeSites.length; i++){
            let site = enzymeSites[i];
            let pattern = "";
            let Npresent = false;
            let Ncount = 0;
            for (let u = 0; u < site.length; u ++){
                let char = site[u];
                if (["A", "T", "G", "C"].includes(char)){
                    pattern += char;
                }
                if (char === "N"){
                    Npresent = true;
                    Ncount ++;
                }
            }
            if (Npresent){
                pattern += "\\w{"+Ncount+"}";
                pattern = new RegExp(pattern, 'g');

                let matchArray = this.sequence.match(pattern);
                try{
                    numberOfMatches += matchArray.length;
                    positionArray = positionArray.concat(this.findPositions(this.sequence, matchArray));
                }catch (e){}
            }
            if (!Npresent){
                positionArray = positionArray.concat(this.findPositions(this.sequence, pattern));
                numberOfMatches = positionArray.length;
            }
        }

        subParagraph += "Number of matches: " + numberOfMatches + "<br>Indexes of matches:<br>"

        positionArray.forEach((position) => {
            subParagraph += position[0] + " to " + position[1] + "<br>";
        })
        document.querySelector("#Errorstats").innerHTML = "";
        return subParagraph + "</p>";
    },

    // Finds all matches in the sequence for a specific match or list of matches. Returns a list with sublists containing
    // the start- and end index of the match.
    findPositions : function (matches){
        let tmpArray = []
        if (typeof matches === "object"){
            for (let i = 0; i < matches.length; i++) {
                for (let u = 0; u < this.sequence.length; u++){
                    if (this.sequence.substring(u, u + matches[i].length) === matches[i]) {
                        tmpArray.push([u, u + matches[i].length]);
                    }
                }
            }
        }
        else {
            for (let u = 0; u < this.sequence.length; u++){
                if (this.sequence.substring(u, u + matches.length) === matches) {
                    tmpArray.push([u, u + matches.length]);
                }
            }
        }
        return tmpArray;
    },

    // resets all object properties to starting conditions;
    reset : function (){
        this.sequence = "ATG";
        this.charOccurrences = {};
    }
}